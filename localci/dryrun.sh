if [ ! -d "../database-migrator" ]; then
    echo "Directory ../database-migrator doesn't exist. Exiting ..."
    exit 1
fi
echo "Recompiling code ..."
docker build -t investment-operator .
cd ../database-migrator
docker build -t database-migrator . || exit 1
cd ../investment-operator
echo "Executing ..."
running_db_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator \
    || exit 1
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db \
    || exit 1
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run fixtures -- -c ormconfig.ts src/fixtures/investment-operator/dryrun \
    || exit 1
docker run \
    --rm \
    --add-host=mysql:$running_db_ip \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    --env-file .env.local \
    -v $(pwd)/logs:/app/logs \
    investment-operator