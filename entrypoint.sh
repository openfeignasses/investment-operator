#!/bin/bash

set -e

# Start in watching mode for production
if [ -n "$KEEP_WATCHING_ALLOCATION_RESULTS" ]; then
    echo "Starting with keep watching allocations_results table mode ..."
    while [ true ]
    do
        java -cp /app/target/investment-operator-1.0-SNAPSHOT-jar-with-dependencies.jar ShouldRun && \
        java -jar /app/target/investment-operator-1.0-SNAPSHOT-jar-with-dependencies.jar && \
        curl --header "Content-Type: multipart/form-data" \
             --request POST -F file=@logs/investment-operator.log \
             https://discord.com/api/webhooks/$WEBHOOK_ID/$WEBHOOK_TOKEN && \
        rm -f logs/investment-operator.log && \
        sleep 480 # sleep an additional 8 minutes if executed.

        sleep 120 # loop every 2 minutes anyway
    done
fi

# By default start with what is define in CMD
exec "$@"