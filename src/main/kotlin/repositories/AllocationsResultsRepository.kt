package repositories

import model.AllocationResult
import utils.PersistenceConfigOverride
import javax.persistence.*

class AllocationsResultsRepository {
    private val entityManager: EntityManager
    private var emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory(
            "PersistenceProviderMysql", PersistenceConfigOverride.configMap)

    init {
        entityManager = emFactory.createEntityManager()
    }

    fun findLastAllocationResults(): List<AllocationResult> {
        val query: TypedQuery<AllocationResult> = entityManager
                .createQuery("SELECT a FROM model.AllocationResult a WHERE a.execution_id = " +
                        "(SELECT MAX(a.execution_id) FROM model.AllocationResult a)",
                        AllocationResult::class.java)

        return query.resultList
    }

    fun persist(allocationResults: List<AllocationResult>) {
        entityManager.transaction?.begin()

        for (allocationResult in allocationResults) {
            entityManager.persist(allocationResult)
        }

        entityManager.transaction?.commit()
    }
}
