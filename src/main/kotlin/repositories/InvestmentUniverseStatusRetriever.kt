package repositories

import model.InvestmentUniverseStatus
import utils.PersistenceConfigOverride
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

class InvestmentUniverseStatusRetriever() {

    private lateinit var investmentUniverseStatusList: List<InvestmentUniverseStatus>
    lateinit var investmentUniverseStatus: InvestmentUniverseStatus
    var entityManager: EntityManager? = null

    fun findByPair(pair: String): InvestmentUniverseStatus {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        val qlQuery = "SELECT i FROM model.InvestmentUniverseStatus i where i.name = :name"
        val query: Query? = entityManager?.createQuery(qlQuery)?.setParameter("name", pair)
        investmentUniverseStatus = query?.singleResult as InvestmentUniverseStatus

        entityManager?.close()
        emFactory.close()

        return investmentUniverseStatus
    }

    fun findAll(): List<InvestmentUniverseStatus> {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        val qlQuery = "SELECT i FROM model.InvestmentUniverseStatus i WHERE i.status != 'FORCE_IGNORE'"
        val query: Query? = entityManager?.createQuery(qlQuery)
        investmentUniverseStatusList = query?.resultList as List<InvestmentUniverseStatus>

        entityManager?.close()
        emFactory.close()

        return investmentUniverseStatusList
    }
}