package repositories

import model.Transaction
import utils.PersistenceConfigOverride
import javax.persistence.*

class TransactionsRepository {
    private val entityManager: EntityManager
    private var emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory(
            "PersistenceProviderMysql", PersistenceConfigOverride.configMap)

    init {
        entityManager = emFactory.createEntityManager()
    }

    fun persist(tradingTransactions: List<Transaction>) {
        entityManager.transaction?.begin()

        for (tradingTransaction in tradingTransactions) {
            entityManager.persist(tradingTransaction)
        }

        entityManager.transaction?.commit()
    }
}
