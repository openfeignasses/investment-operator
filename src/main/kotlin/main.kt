import org.apache.logging.log4j.LogManager
import utils.PersistenceConfigOverride
import utils.Utils

fun main() {
    PersistenceConfigOverride.setDatabaseConnetion(
            System.getenv("DATABASE_HOST"),
            System.getenv("DATABASE_PORT"),
            System.getenv("MYSQL_DATABASE"),
            System.getenv("MYSQL_USER"),
            System.getenv("MYSQL_PASSWORD"),
            "false"
    )
    val logger = LogManager.getLogger()
    val utils = Utils()

    logger.info("+++++++++++++++++ EXÉCUTION DE L'INVESTMENT OPERATOR +++++++++++++++++++")

    InvestmentOperator().run(utils, logger)
}
