package model

import org.knowm.xchange.currency.Currency
import java.math.BigDecimal

class Delta(var currency: Currency, var value: BigDecimal) {
    override fun toString(): String {
        return "Currency: $currency, Value: $value"
    }
}