package model

import utils.NoArg
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity @NoArg
@Table(name="allocations_results")
class AllocationResult(currency: String, valuePercent: Double, allocationName: String) {
    @Id
    @GeneratedValue
    val id: Int = 0
    val currency: String = currency
    val value_percent: Double? = valuePercent
    val allocation_name: String = allocationName
    val execution_id: Int = 1
    val add_date: Long = java.util.Date().toInstant().epochSecond
}
