package model

class Trader (
    var name: String,
    var apiKey: String = "",
    var secretKey: String = ""
)
