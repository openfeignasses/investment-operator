import kraken.KrakenConnector
import model.Delta
import model.Trader
import model.Transaction
import org.apache.logging.log4j.Logger
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.exceptions.ExchangeException
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import repositories.InvestmentUniverseStatusUpdater
import repositories.TransactionsRepository
import utils.TransactionBuilder
import java.math.BigDecimal
import java.math.RoundingMode

class OrdersExecutor(traders: List<Trader>, operationsToPerform: MutableMap<String, java.util.ArrayList<Delta>>, logger: Logger, lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>) {

    init {
        executeOrders(traders, operationsToPerform, logger, lastPrices)
    }

    fun executeOrders(traders: List<Trader>, operationsToPerform: MutableMap<String, java.util.ArrayList<Delta>>, logger: Logger, lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>) {
        val krakenConnector = KrakenConnector()
        var successfulTransactions: ArrayList<Transaction> = arrayListOf()
        executeForAllTraders(traders, operationsToPerform, logger, krakenConnector, lastPrices, successfulTransactions)
        logger.info("============ TRANSACTIONS À ENREGISTRER =============")
        saveTransactions(successfulTransactions, logger)
    }

    private fun saveTransactions(successfulTransactions: ArrayList<Transaction>, logger: Logger) {
        successfulTransactions.forEach { successfulTransaction ->
            logger.info("successfulTransaction: " + successfulTransaction)
        }

        var successfulTransactionsWithoutDuplicates = arrayListOf<Transaction>()
        successfulTransactions.forEach { successfulTransaction ->
            if (!successfulTransactionsWithoutDuplicates.contains(successfulTransaction)) {
                successfulTransactionsWithoutDuplicates.add(successfulTransaction)
            }
        }

        successfulTransactionsWithoutDuplicates.forEach { successfulTransactionWithoutDuplicates ->
            logger.info("successfulTransactionsWithoutDuplicates: " + successfulTransactionWithoutDuplicates)
        }

        TransactionsRepository().persist(successfulTransactionsWithoutDuplicates)
    }

    private fun executeForAllTraders(traders: List<Trader>, operationsToPerform: MutableMap<String, java.util.ArrayList<Delta>>, logger: Logger, krakenConnector: KrakenConnector, lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>, successfulTransactions: ArrayList<Transaction>) {
        for (registeredTrader in traders) {
            val sells = mutableListOf<Delta>()
            val buys = mutableListOf<Delta>()

            performOperations(operationsToPerform, logger, registeredTrader, sells, buys, krakenConnector, lastPrices, successfulTransactions)
        }
    }

    private fun performOperations(operationsToPerform: MutableMap<String, java.util.ArrayList<Delta>>, logger: Logger, registeredTrader: Trader, sells: MutableList<Delta>, buys: MutableList<Delta>, krakenConnector: KrakenConnector, lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>, successfulTransactions: ArrayList<Transaction>) {
        operationsToPerform.forEach { trader, deltas ->
            logger.info("============ EXÉCUTION DES OPÉRATIONS =============")
            logger.info("Opérations pour $trader:")
            if (registeredTrader.name == trader) {
                deltas.forEach { delta ->
                    if (delta.value < BigDecimal.ZERO) {
                        sells.add(delta)
                    } else {
                        buys.add(delta)
                    }
                }

                var estimatedSellFees = BigDecimal.ZERO;
                var sellErrored = false
                sells.forEach { delta ->
                    logger.info("   Vente de  ${-delta.value}: ${CurrencyPair(delta.currency, Currency.EUR)} ")
                    try {
                        krakenConnector.executeSell(registeredTrader, CurrencyPair(delta.currency, Currency.EUR), -delta.value)
                        val latestClosePrice = lastPrices[CurrencyPair(delta.currency, Currency.EUR)]!!.close
                        estimatedSellFees += BigDecimal(0.0026) * delta.value * latestClosePrice
                        successfulTransactions.add(TransactionBuilder(delta, latestClosePrice).transaction)
                        InvestmentUniverseStatusUpdater(delta.currency.toString() + "_EUR", "TRADABLE")
                    } catch (e: ExchangeException) {
                        sellErrored = true
                        logger.error("   Erreur durant la vente...")
                        logger.error(e.message)
                    }
                }

                if (!sellErrored) {
                    buys.forEach { delta ->
                        val finalVolume = (delta.value * BigDecimal(0.99)).setScale(8, RoundingMode.FLOOR)
                        logger.info("   Achat de ${finalVolume}: ${CurrencyPair(delta.currency, Currency.EUR)}")
                        try {
                            krakenConnector.executeBuy(registeredTrader, CurrencyPair(delta.currency, Currency.EUR), finalVolume)
                            val latestClosePrice = lastPrices[CurrencyPair(delta.currency, Currency.EUR)]!!.close
                            successfulTransactions.add(TransactionBuilder(delta, latestClosePrice).transaction)
                            InvestmentUniverseStatusUpdater(delta.currency.toString() + "_EUR", "TRADABLE")
                        } catch (e: ExchangeException) {
                            logger.info("   Exception durant l'achat...")
                            logger.info(e.message)
                        }
                    }
                } else {
                    logger.error("   Erreur pendant l'achat, toutes ventes annulées.")
                }
            }
        }
    }
}


