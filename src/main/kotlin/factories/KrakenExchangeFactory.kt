package factories

import model.Trader
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.ExchangeSpecification
import org.knowm.xchange.kraken.KrakenExchange

object KrakenExchangeFactory {
    var httpConnTimeout: Int = 10000
    var httpReadTimeout: Int = 10000

    fun createExchange(): KrakenExchange {
        val specification = ExchangeSpecification(KrakenExchange::class.java.name)
        if (System.getenv("MOCK_KRAKEN_API") == "true") {
            specification.host = "localhost"
            specification.port = 8080
            specification.sslUri = "http://localhost:8080"
        }
        specification.httpConnTimeout = httpConnTimeout
        specification.httpReadTimeout = httpReadTimeout

        return ExchangeFactory.INSTANCE.createExchange(specification) as KrakenExchange
    }

    fun createExchange(trader: Trader): KrakenExchange {
        val specification = ExchangeSpecification(KrakenExchange::class.java.name)
        if (System.getenv("MOCK_KRAKEN_API") == "true") {
            specification.host = "localhost"
            specification.port = 8080
            specification.sslUri = "http://localhost:8080"
        }
        specification.httpConnTimeout = httpConnTimeout
        specification.httpReadTimeout = httpReadTimeout
        specification.apiKey = trader.apiKey
        specification.secretKey = trader.secretKey

        return ExchangeFactory.INSTANCE.createExchange(specification) as KrakenExchange
    }
}
