import com.github.jasync.sql.db.util.length
import model.AllocationResult
import model.Delta
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Wallet
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import java.math.BigDecimal

class InvestmentCalculator(
        private val wallet: Wallet,
        private val minimumOrderSizes: Map<Currency, BigDecimal>,
        private val ohlcs: MutableMap<CurrencyPair, KrakenOHLC?>,
        private val allocationResults: Map<Currency, AllocationResult>
) {
    companion object {
        val fiatCurrencies: ArrayList<Currency> = arrayListOf(Currency.EUR)
    }

    private var tradableCurrencies: MutableSet<Currency> = mutableSetOf()
    var operations: ArrayList<Delta> = arrayListOf()
        private set

    init {
        computeInvestments()
    }

    private fun computeInvestments() {
        tradableCurrencies = mutableSetOf()
        operations = arrayListOf()
        allocationResults.forEach {
            tradableCurrencies.add(it.key)
        }

        correctTradableCurrencies()

        if (tradableCurrencies.length == 0) {
            return
        }

        val walletValue = getWalletValue()
        val finalValues = getFinalValues(walletValue)

        /**
         * First Compute the deltas for each currencies of the wallet
         * Including new currencies dues to investments
         * We ensured earlier that they all have enough funds
         */
        // First the currencies we keep
        finalValues.forEach {
            var startValue = BigDecimal.ZERO
            if (wallet.balances.containsKey(it.key)) {
                startValue = wallet.balances[it.key]!!.available
            }
            val deltaValue = it.value - startValue

            if (deltaValue < BigDecimal.ZERO) {
                // Base value is the crypto currency
                if (deltaValue.abs() < minimumOrderSizes[it.key]?.abs()) {
                    return@forEach
                }
            } else {
                // Base value is the fiat currency
                val fiatCurrencyToSpend = deltaValue * ohlcs[CurrencyPair(it.key, Currency.EUR)]!!.close
                if (fiatCurrencyToSpend < minimumOrderSizes[Currency.EUR]?.abs()) {
                    return@forEach
                }
            }

            val delta = Delta(it.key, deltaValue)
            operations.add(delta)
        }
        /**
         * Then add the deltas for the currencies that should be entirely sold
         * We ensured earlier that they all have enough funds
         */
        wallet.balances.forEach {
            if (!tradableCurrencies.contains(it.key)) {
                return@forEach
            }

            if (!finalValues.containsKey(it.key) && it.value.available > BigDecimal.ZERO) {
                val deltaValue = - it.value.available
                val delta = Delta(it.key, deltaValue)
                operations.add(delta)
            }
        }
    }

    /**
     * Correct the list of tradable currencies
     * - By removing currencies for which an OHLC couldn't be retrieved
     * - By removing currencies for which an allocation result wasn't found
     * - By removing currencies that should be sold but which balance is to low
     * - By removing currencies for which we don't have a minimum order size
     * - By removing currencies which should be partially sold but the delta is below the minimum order size
     */
    private fun correctTradableCurrencies() {
        val currenciesToRemove = mutableSetOf<Currency>()
        tradableCurrencies.forEach {
            // No minimum order size
            if (!minimumOrderSizes.containsKey(it)) {
                currenciesToRemove.add(it)
                return@forEach
            }
            // No allocation result
            if (!allocationResults.containsKey(it)) {
                currenciesToRemove.add(it)
                return@forEach
            }
            // No OHLC
            if (!ohlcs.containsKey(CurrencyPair(it, Currency.EUR))) {
                currenciesToRemove.add(it)
                return@forEach
            }
        }

        println("/!\\ currenciesToRemove: $currenciesToRemove")
        currenciesToRemove.forEach {
            tradableCurrencies.remove(it)
        }

        // Balance is too low to sell all.
        wallet.balances.forEach {
            if (!tradableCurrencies.contains(it.key)) {
                return@forEach
            }

            if (allocationResults[it.key]!!.value_percent == 0.0) {
                if (it.value.available < minimumOrderSizes[it.key]) {
                    tradableCurrencies.remove(it.key)
                }
            }
        }

        // Delta is too low to sell
        val walletValue = getWalletValue()
        val finalValues = getFinalValues(walletValue)

        finalValues.forEach {
            if (!wallet.balances.containsKey(it.key)) {
                return@forEach
            }

            val startValue = wallet.balances[it.key]!!.available
            val deltaValue = it.value - startValue

            if (deltaValue < BigDecimal.ZERO && deltaValue.abs() < minimumOrderSizes[it.key]?.abs()) {
                tradableCurrencies.remove(it.key)
            }
        }
    }

    /**
     * Evaluate the value of the wallet in Euros
     */
    private fun getWalletValue(): BigDecimal {
        var walletValue = BigDecimal.ZERO
        wallet.balances.forEach {
            if (!tradableCurrencies.contains(it.key)) {
                return@forEach
            }
            walletValue += ohlcs[CurrencyPair(it.key, Currency.EUR)]!!.close * it.value.available
        }

        fiatCurrencies.forEach {
            if (!wallet.balances.containsKey(it)) {
                return@forEach
            }
            walletValue += wallet.balances[it]!!.available
        }

        return walletValue
    }

    /**
     * Compute which amount of which currency should be in the wallet in the final state
     */
    private fun getFinalValues(walletValue: BigDecimal): Map<Currency, BigDecimal> {
        val finalValues = HashMap<Currency, BigDecimal>()
        allocationResults.forEach {
            if (!tradableCurrencies.contains(it.key)) {
                return@forEach
            }
            finalValues[it.key] = walletValue * BigDecimal(allocationResults[it.key]!!.value_percent!!) / ohlcs[CurrencyPair(it.key, Currency.EUR)]!!.close
        }

        return finalValues
    }
}