package utils

import model.Delta
import model.Transaction
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import java.math.BigDecimal

class TransactionBuilder(delta: Delta, latestClosePrice: BigDecimal) {

    val transaction: Transaction

    init {

        val orderType: String
        if (delta.value < BigDecimal.ZERO) {
            orderType = "SELL"
        }
        else {
            orderType = "BUY"
        }

        transaction = Transaction(
                currency_pair = delta.currency.toString() + "_" + Currency.EUR,
                order_type = orderType,
                price = latestClosePrice
        )
    }
}