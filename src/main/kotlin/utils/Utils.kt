package utils

import InvestmentCalculator
import kraken.KrakenConnector
import model.AllocationResult
import model.Delta
import model.Trader
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Wallet
import org.knowm.xchange.exceptions.ExchangeException
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import repositories.AllocationsResultsRepository
import java.io.BufferedReader
import java.io.FileReader
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

object Constants {
    const val COMMA_DELIMITER = ','
}

class Utils {
    private val logger: Logger = LogManager.getLogger()

    fun getLastPrices(allocationResultsMap: HashMap<Currency, AllocationResult>): MutableMap<CurrencyPair, KrakenOHLC?> {
        val krakenConnector = KrakenConnector()
        krakenConnector.setUp()
        val lastPrices: MutableMap<CurrencyPair, KrakenOHLC?> = mutableMapOf()
        allocationResultsMap.forEach { (currency, _) ->
            if (InvestmentCalculator.fiatCurrencies.contains(currency)) {
                return@forEach
            }
            var currencyPair = CurrencyPair(currency.currencyCode+"/EUR")
            lastPrices[currencyPair] = krakenConnector.getLastPrice(currencyPair)!!
        }
        return lastPrices
    }

    fun getMinimumOrdersizes(): MutableMap<Currency, BigDecimal> {
        // CSV can be found here : https://support.kraken.com/hc/en-us/articles/360042589912-Order-Minimums-Deposit-Withdrawal-Minimums-etc-
        val minimumOrderSizes: MutableMap<Currency, BigDecimal> = mutableMapOf()
        BufferedReader(FileReader("src/main/resources/Order_Minimums.csv")).use { br ->
            // Throw first CSV line in the void, bye
            br.readLine()
            var line = br.readLine()
            while (line != null) {
                val values: Array<String> = line.split(Constants.COMMA_DELIMITER).toTypedArray()
                try {
                    val baseCurrency = Currency(values[0])
                    val minimumOrderSize = BigDecimal(values[1].toDouble()).setScale(8, RoundingMode.HALF_UP)
                    minimumOrderSizes[baseCurrency] = minimumOrderSize
                } catch (e: Exception) {
                    logger.warn("Unable to parse minimum amount line in csv : $line")
                } finally {
                    line = br.readLine()
                }
            }
        }
        return minimumOrderSizes
    }

    fun fetchFees() {
        val krakenConnector = KrakenConnector()
        krakenConnector.setUp()
        val assetPairsMap = krakenConnector.getFees()

        logger.info("=======================================================================")
        assetPairsMap.forEach { key, assetPair ->
            logger.info("$key: ${assetPair.base}/${assetPair.fees}/${assetPair.fees_maker}\n")
        }
    }

    fun getOperationsToPerform(walletsComposition: MutableMap<String, Wallet>,
                               minimumOrderSizes: MutableMap<Currency, BigDecimal>,
                               lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>,
                               allocationResultsMap: HashMap<Currency, AllocationResult>)
            :MutableMap<String, ArrayList<Delta>> {

        val operationsToPerform: MutableMap<String, ArrayList<Delta>> = mutableMapOf()

        walletsComposition.forEach { (traderName, wallet) ->
            val investmentCalculator = InvestmentCalculator(
                    wallet,
                    minimumOrderSizes,
                    lastPrices,
                    allocationResultsMap
            )

            operationsToPerform[traderName] = investmentCalculator.operations
        }

        return operationsToPerform
    }

    fun getAllocationsResultsMap(): HashMap<Currency, AllocationResult> {
        val allocationResultMap = HashMap<Currency, AllocationResult>()
        val allocationsResultsList: List<AllocationResult> = AllocationsResultsRepository().findLastAllocationResults()

        allocationsResultsList.forEach {
            allocationResultMap[Currency(it.currency)] = it
        }

        return allocationResultMap
    }

    fun getWalletsComposition(traders: List<Trader>): MutableMap<String, Wallet> {

        val krakenConnector = KrakenConnector()
        val wallets: MutableMap<String, Wallet> = mutableMapOf()

        traders.forEach { trader ->
            try {
                val traderWallet = krakenConnector.fetchWalletComposition(trader)
                        ?.filter { it.value.id != null }!!["margin"] ?: error("No margin wallet found.")
                wallets[trader.name] = traderWallet
            } catch (e: ExchangeException) {
                logger.info("Couldn't fetch wallet composition for trader ${trader.name}. Skipping.")
            }
        }

        return wallets
    }

    fun getTraders(): List<Trader> {
        val traders = emptyList<Trader>().toMutableList()
        if (System.getenv("API_KEY_SARTAROTH") != null && System.getenv("SECRET_KEY_SARTAROTH") != null) {
            traders.add(Trader(
                    "Sartaroth",
                    System.getenv("API_KEY_SARTAROTH"),
                    System.getenv("SECRET_KEY_SARTAROTH")
            ))
        } else {
            logger.info("Access keys for Sartaroth not defined, skipping.")
        }
        if (System.getenv("API_KEY_RUSTPATCH") != null && System.getenv("SECRET_KEY_RUSTPATCH") != null) {
            traders.add(Trader(
                    "Rustpatch",
                    System.getenv("API_KEY_RUSTPATCH"),
                    System.getenv("SECRET_KEY_RUSTPATCH")
            ))
        } else {
            logger.info("Access keys for Rustpatch not defined, skipping.")
        }
        if (System.getenv("API_KEY_TONY") != null && System.getenv("SECRET_KEY_TONY") != null) {
            traders.add(Trader(
                    "Tony",
                    System.getenv("API_KEY_TONY"),
                    System.getenv("SECRET_KEY_TONY")
            ))
        } else {
            logger.info("Access keys for Tony not defined, skipping.")
        }
        if (System.getenv("API_KEY_DJOU") != null && System.getenv("SECRET_KEY_DJOU") != null) {
            traders.add(Trader(
                    "Djou",
                    System.getenv("API_KEY_DJOU"),
                    System.getenv("SECRET_KEY_DJOU")
            ))
        } else {
            logger.info("Access keys for Djou not defined, skipping.")
        }
        if (System.getenv("API_KEY_GORGOROTH") != null && System.getenv("SECRET_KEY_GORGOROTH") != null) {
            traders.add(Trader(
                    "Gorgoroth",
                    System.getenv("API_KEY_GORGOROTH"),
                    System.getenv("SECRET_KEY_GORGOROTH")
            ))
        } else {
            logger.info("Access keys for Gorgoroth not defined, skipping.")
        }

        return traders.toList()
    }
}