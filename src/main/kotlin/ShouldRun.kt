import org.joda.time.DateTime
import repositories.AllocationsResultsRepository
import utils.PersistenceConfigOverride
import kotlin.system.exitProcess

object ShouldRun {
    @JvmStatic
    fun main(args: Array<String>) {
        PersistenceConfigOverride.setDatabaseConnetion(
                System.getenv("DATABASE_HOST"),
                System.getenv("DATABASE_PORT"),
                System.getenv("MYSQL_DATABASE"),
                System.getenv("MYSQL_USER"),
                System.getenv("MYSQL_PASSWORD"),
                "false"
        )

        val maxAddDate = AllocationsResultsRepository().findLastAllocationResults()[0].add_date

        if (maxAddDate == null) {
            println(DateTime.now().toString() + " | Should the program run ? Answer: yes")
            exitProcess(0)
        }
        else {
            val maxAddDate = maxAddDate
            val tenMinutesSeconds = 600
            val currentTimestampSeconds = DateTime.now().millis / 1000
            if (maxAddDate > currentTimestampSeconds - tenMinutesSeconds.toLong()) {
                println(DateTime.now().toString() + " | Should the program run ? Answer: yes")
                exitProcess(0)
            }
        }
        println(DateTime.now().toString() + " | Should the program run ? Answer: no")
        exitProcess(1)
    }
}