package kraken
import factories.KrakenExchangeFactory
import model.Trader
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.Order
import org.knowm.xchange.dto.account.Wallet
import org.knowm.xchange.dto.trade.MarketOrder
import org.knowm.xchange.kraken.KrakenExchange
import org.knowm.xchange.kraken.dto.marketdata.KrakenAssetPair
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import org.knowm.xchange.kraken.service.KrakenMarketDataServiceRaw
import java.math.BigDecimal


class KrakenConnector {

    private lateinit var krakenExchange: KrakenExchange
    var httpConnTimeout: Int = 10000
    var httpReadTimeout: Int = 10000

    fun setUp() {
        krakenExchange = KrakenExchangeFactory.createExchange();
    }

    fun fetchWalletComposition(trader: Trader): MutableMap<String, Wallet>? {
        val privateKrakenExchange = KrakenExchangeFactory.createExchange(trader);
        val accountService = privateKrakenExchange.accountService
        return accountService.accountInfo.wallets
    }

    fun getLastPrice(currencyPair: CurrencyPair): KrakenOHLC? {
        val marketDataServiceRaw = krakenExchange.marketDataService as KrakenMarketDataServiceRaw
        val ohlcs = marketDataServiceRaw.getKrakenOHLC(currencyPair, 1, 99999999999)
        return ohlcs.ohlCs.last()
    }

    fun executeSell(trader: Trader, currencyPair: CurrencyPair, delta: BigDecimal) {
        val privateKrakenExchange = KrakenExchangeFactory.createExchange(trader);
        val tradeService = privateKrakenExchange.tradeService
        tradeService.placeMarketOrder(MarketOrder(Order.OrderType.ASK, delta, currencyPair))
    }

    fun executeBuy(trader: Trader, currencyPair: CurrencyPair, delta: BigDecimal) {
        val privateKrakenExchange = KrakenExchangeFactory.createExchange(trader);
        val tradeService = privateKrakenExchange.tradeService
        tradeService.placeMarketOrder(MarketOrder(Order.OrderType.BID, delta, currencyPair))
    }

    fun getFees(): Map<String, KrakenAssetPair> {
        val marketDataServiceRaw = krakenExchange.marketDataService as KrakenMarketDataServiceRaw
        val krakenAssetPairs = marketDataServiceRaw.getKrakenAssetPairs(CurrencyPair.BTC_EUR)
        return krakenAssetPairs.assetPairMap
    }
}
