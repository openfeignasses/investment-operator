import model.AllocationResult
import model.Delta
import model.Trader
import org.apache.logging.log4j.Logger
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Wallet
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import utils.Utils
import java.math.BigDecimal
import java.util.HashMap

class InvestmentOperator {
    fun run(utils: Utils, logger: Logger) {
        val traders: List<Trader> = utils.getTraders()
        val walletsComposition = utils.getWalletsComposition(traders)
        val allocationResultsMap = utils.getAllocationsResultsMap()
        val lastPrices: MutableMap<CurrencyPair, KrakenOHLC?> = utils.getLastPrices(allocationResultsMap)
        val minimumOrderSizes: MutableMap<Currency, BigDecimal> = utils.getMinimumOrdersizes()
        val operationsToPerform = utils.getOperationsToPerform(walletsComposition, minimumOrderSizes, lastPrices, allocationResultsMap)

        debug(logger, walletsComposition, allocationResultsMap, lastPrices, minimumOrderSizes, operationsToPerform)

        OrdersExecutor(traders, operationsToPerform, logger, lastPrices)
    }
}

private fun debug(logger: Logger, walletsComposition: MutableMap<String, Wallet>, allocationResultsMap: HashMap<Currency, AllocationResult>, lastPrices: MutableMap<CurrencyPair, KrakenOHLC?>, minimumOrderSizes: MutableMap<Currency, BigDecimal>, operationsToPerform: MutableMap<String, java.util.ArrayList<Delta>>) {
    logger.info("=========== COMPOSITION DES WALETS ============")
    walletsComposition.forEach { trader, wallet ->
        logger.info("Wallet de $trader:")
        wallet.balances.forEach { currency, balance ->
            logger.info("    $currency: ${balance.available}")
        }
    }
    logger.info("============ RÉSULTATS DE L'ALLOCATION =============")
    allocationResultsMap.forEach { currency, allocationResult ->
        logger.info("$currency: ${allocationResult.value_percent}")
    }
    logger.info("============ DERNIERS PRIX =============")
    lastPrices.forEach { currencyPair, ohlc ->
        logger.info("${currencyPair.base}/${currencyPair.counter}: ${ohlc?.close}")
    }
    logger.info("============ TAILLE MINIMALE DES ORDRES =============")
    minimumOrderSizes.forEach { currency, size ->
        logger.info("$currency: $size")
    }
    logger.info("============ OPÉRATION À EFFECTUER =============")
    operationsToPerform.forEach { trader, deltas ->
        logger.info("Opérations pour $trader:")
        deltas.forEach { delta ->
            logger.info("    ${delta.currency}: ${delta.value}")
        }
    }
}