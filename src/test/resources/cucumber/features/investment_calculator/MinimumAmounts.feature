@InvestmentCalculator
@InvestmentCalculator_MinimumAmounts
@PlatformIndependent
@Stub
Feature: Calculate the final wallet composition

   As _ the investment calculator
   In order to transform allocations in real investment values
   I want to translate the relative allocations in absolute values
   according to the current wallet value

   @InvestmentCalculator_MinimumAmounts_1
   Scenario: One account, one crypto-currency
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the "EUR" allocation is "0.0"
      And the "BTC" allocation is "1.0"
      And the minimum order size for "BTC" is "0"
      And the minimum order size for "EUR" is "101"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "100" for "Julien"`s wallet
      Then the final amount of "BTC" is "0" for "Julien"`s wallet

   @InvestmentCalculator_MinimumAmounts_2
   Scenario: One account, two currencies
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the current "ETH_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the initial amount of "ETH" is "0" for "Julien"`s wallet
      And the "EUR" allocation is "0.0"
      And the "BTC" allocation is "0.5"
      And the "ETH" allocation is "0.5"
      And the minimum order size for "BTC" is "0"
      And the minimum order size for "ETH" is "0"
      And the minimum order size for "EUR" is "51"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "100" for "Julien"`s wallet
      Then the final amount of "BTC" is "0" for "Julien"`s wallet
      Then the final amount of "ETH" is "0" for "Julien"`s wallet

   @InvestmentCalculator_MinimumAmounts_3
   Scenario: One account, two currencies, different min. amounts
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the current "ETH_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the initial amount of "ETH" is "0" for "Julien"`s wallet
      And the "EUR" allocation is "0.0"
      And the "BTC" allocation is "0.52"
      And the "ETH" allocation is "0.48"
      And the minimum order size for "BTC" is "0"
      And the minimum order size for "ETH" is "0"
      And the minimum order size for "EUR" is "51"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "48" for "Julien"`s wallet
      Then the final amount of "BTC" is "52" for "Julien"`s wallet
      Then the final amount of "ETH" is "0" for "Julien"`s wallet

   @InvestmentCalculator_MinimumAmounts_4
   Scenario: One account, 3 currencies, realistic values
      Given I have "Tony" registered as a trader
      And the current "BTC_EUR" value is "19398.5"
      And the current "REP_EUR" value is "14.348"
      And the current "XMR_EUR" value is "134.75"
      And the initial amount of "EUR" is "160.0245" for "Tony"`s wallet
      And the initial amount of "BTC" is "0" for "Tony"`s wallet
      And the initial amount of "REP" is "0" for "Tony"`s wallet
      And the initial amount of "XMR" is "0" for "Tony"`s wallet
      And the "EUR" allocation is "0.0"
      And the "BTC" allocation is "0.472372"
      And the "REP" allocation is "0.055256"
      And the "XMR" allocation is "0.472372"
      And the minimum order size for "BTC" is "0.001"
      And the minimum order size for "REP" is "0.3"
      And the minimum order size for "XMR" is "0.1"
      And the minimum order size for "EUR" is "10"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "8.842313772" for "Tony"`s wallet
      Then the final amount of "BTC" is "0.003896749" for "Tony"`s wallet
      Then the final amount of "REP" is "0" for "Tony"`s wallet
      Then the final amount of "XMR" is "0.560972862" for "Tony"`s wallet
