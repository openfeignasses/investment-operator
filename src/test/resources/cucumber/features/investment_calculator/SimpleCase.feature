@InvestmentCalculator
@InvestmentCalculator_SimpleCase
@PlatformIndependent
@Stub
Feature: Calculate the final wallet composition

   As _ the investment calculator
   In order to transform allocations in real investment values
   I want to translate the relative allocations in absolute values
   according to the current wallet value

   @InvestmentCalculator_SimpleCase_1
   Scenario: One account, one crypto-currency
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the "EUR" allocation is "0.5"
      And the "BTC" allocation is "0.5"
      And the minimum order size for "BTC" is "1"
      And the minimum order size for "EUR" is "1"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "50" for "Julien"`s wallet
      Then the final amount of "BTC" is "50" for "Julien"`s wallet

   @InvestmentCalculator_SimpleCase_2
   Scenario: One account, two currencies
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the current "ETH_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the initial amount of "ETH" is "0" for "Julien"`s wallet
      And the "EUR" allocation is "0.0"
      And the "BTC" allocation is "0.5"
      And the "ETH" allocation is "0.5"
      And the minimum order size for "BTC" is "1"
      And the minimum order size for "ETH" is "1"
      And the minimum order size for "EUR" is "1"
      When I translate the allocation into absolute values
      Then the final amount of "EUR" is "0" for "Julien"`s wallet
      Then the final amount of "BTC" is "50" for "Julien"`s wallet
      Then the final amount of "ETH" is "50" for "Julien"`s wallet