@InvestmentCalculator
@InvestmentCalculator_ServerAndNetworkErrors
@PlatformIndependent
@Stub
Feature: Server and network errors

   As _ the investment calculator 
   In order to transform allocations in real investment values
   I want to properly communicate with the configured platform.

   @InvestmentCalculator_ServerError_1
   Scenario: Communication error
      Given I have "Julien" registered as a trader
      And the current "BTC_EUR" value is "1"
      And the initial amount of "EUR" is "100" for "Julien"`s wallet
      And the initial amount of "BTC" is "0" for "Julien"`s wallet
      And the "BTC" allocation is "0.5"
      And the minimum order size for "BTC" is "1"
      And the minimum order size for "EUR" is "1"
      And the server is not returning a response
      And the request timeout is 10 seconds
      When I translate the allocation into absolute values
      Then an error is thrown
