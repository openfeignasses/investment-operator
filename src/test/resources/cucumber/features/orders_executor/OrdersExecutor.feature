@OrdersExecutor
@PlatformIndependent
@Stub
Feature: Execute orders

   As _ the investment calculator
   In order to re-balance wallets
   I want to reuse real investment values
   and execute orders

   @OrdersExecutor_1
   Scenario: Server available
      Given the server is available
      And I have "Julien" registered as a trader
      And the "BTC" delta is "50" for "Julien"`s wallet
      And the current "BTC_EUR" value is "1"
      When I execute orders
      Then "Julien"`s wallet has been re-balanced

   @OrdersExecutor_2
   Scenario: Communication error
      Given the server is not returning a response
      And the request timeout is 10 seconds
      And I have "Julien" registered as a trader
      And the "BTC" delta is "50" for "Julien"`s wallet
      And the current "BTC_EUR" value is "1"
      When I execute orders
      Then an error is thrown

   @OrdersExecutor_3
   Scenario: Currency was marked as crashing
      Given the server is available
      And I have "Julien" registered as a trader
      And the "BTC" delta is "-50" for "Julien"`s wallet
      And the current "BTC_EUR" value is "1"
      And the "BTC_EUR" currency is marked as "CRASHING"
      When I execute orders
      Then "Julien"`s wallet has been re-balanced
      And the "BTC_EUR" currency is flagged as "TRADABLE"