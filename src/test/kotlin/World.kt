import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.google.gson.JsonObject
import model.Delta
import model.Trader
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Wallet
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import java.math.BigDecimal


class World {
    var initalAmountsMocks: MutableMap<String, JsonObject?> = mutableMapOf()
    var walletRebalanced: MutableMap<String, Boolean> = mutableMapOf()
    var deltas: MutableMap<String, ArrayList<Delta>> = mutableMapOf()
    var initialWallets: MutableMap<String, Wallet> = mutableMapOf()
    var minimumOrderSizes: MutableMap<Currency, BigDecimal> = mutableMapOf()
    val ohlcs: MutableMap<CurrencyPair, KrakenOHLC?> = mutableMapOf()
    lateinit var trader: Trader
    lateinit var stubResponses: Map<String, Map<Int, ResponseDefinitionBuilder>>
    var errorOccured: Boolean = false
    lateinit var wireMockServer: WireMockServer
}