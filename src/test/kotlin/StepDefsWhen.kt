import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.*
import cucumber.api.java8.En
import kraken.KrakenConnector
import model.AllocationResult
import model.Trader
import org.apache.logging.log4j.LogManager
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import repositories.AllocationsResultsRepository

class StepDefsWhen(var world: World, val krakenConnector: KrakenConnector, allocationsResultsRepository: AllocationsResultsRepository): En {

    init {

        When("I translate the allocation into absolute values") {

            // Last Allocation Result for BTC
            val allocationsResultsList: List<AllocationResult> = allocationsResultsRepository.findLastAllocationResults()

            // Wallet composition for all accounts
            world.wireMockServer.stubFor(post(urlEqualTo("/0/private/TradeBalance"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody("{\n" +
                                            "   \"error\":[\n" +
                                            "      \n" +
                                            "   ],\n" +
                                            "   \"result\":{\n" +
                                            "      \"eb\":\"687.6885\",\n" +
                                            "      \"tb\":\"670.6865\",\n" +
                                            "      \"m\":\"0.0000\",\n" +
                                            "      \"n\":\"0.0000\",\n" +
                                            "      \"c\":\"0.0000\",\n" +
                                            "      \"v\":\"0.0000\",\n" +
                                            "      \"e\":\"670.6865\",\n" +
                                            "      \"mf\":\"670.6865\"\n" +
                                            "   }\n" +
                                            "}")
                    )
            )

            for ((currentTraderName, jsonBody) in world.initalAmountsMocks) {
                world.wireMockServer.stubFor(WireMock.post(WireMock.urlEqualTo("/0/private/Balance"))
                        .willReturn(
                                aResponse()
                                        ?.withHeader("Content-Type", "application/json")
                                        ?.withBody(jsonBody.toString())
                        )
                )

                val currentTrader = Trader(
                        currentTraderName,
                        "mSWSjO3xUBZYPBQCt1pB8T9LyLoY1JA39h4uHi8GQY5dbR+Eih/zj+nF",
                        "kt8Q3kpenQ630Bevd6XhU2xA34/QQsZDoP20wnys6G2ctIF+6XXqM+FLsfAY1PWfeFcqbpDhe7OI+9W+HTV39Q=="
                )
                val wallet = krakenConnector.fetchWalletComposition(currentTrader)
                        ?.filter { it.value.id != null }!!["margin"] ?: error("No margin wallet found.")

                world.initialWallets[currentTraderName] = wallet
            }

            val allocationResultMap = HashMap<Currency, AllocationResult>()
            allocationsResultsList.forEach {
                allocationResultMap[Currency(it.currency)] = it
            }

            try {
                world.ohlcs.forEach { (currencyPair, _) ->
                    world.ohlcs[currencyPair] = krakenConnector.getLastPrice(currencyPair)!!
                }

                world.initialWallets.forEach { (traderName, wallet) ->
                    val investmentCalculator = InvestmentCalculator(
                            wallet,
                            world.minimumOrderSizes,
                            world.ohlcs,
                            allocationResultMap
                    )

                    world.deltas[traderName] = investmentCalculator.operations
                }
            } catch (e: Exception) {
                e.printStackTrace()
                world.errorOccured = true
            }
        }

        When("I execute orders") {
            val logger = LogManager.getLogger()
            logger.info(listOf(this.world.trader))
            logger.info(world.deltas)

            try {
                world.ohlcs.forEach { (currencyPair, _) ->
                    world.ohlcs[currencyPair] = krakenConnector.getLastPrice(currencyPair)!!
                }

                OrdersExecutor(listOf(this.world.trader), world.deltas, logger, world.ohlcs)
                world.walletRebalanced[this.world.trader.name] = true
            } catch (e: Exception) {
                e.printStackTrace()
                world.errorOccured = true
            }

        }
    }
}