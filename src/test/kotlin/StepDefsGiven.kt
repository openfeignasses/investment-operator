import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.*
import cucumber.api.java8.En
import factories.KrakenExchangeFactory
import kraken.KrakenConnector
import model.AllocationResult
import model.Delta
import model.Trader
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import repositories.AllocationsResultsRepository
import repositories.InvestmentUniverseStatusUpdater
import java.math.BigDecimal
import java.math.RoundingMode


class StepDefsGiven(var world: World, val krakenConnector: KrakenConnector): En {

    init {

        Given("the request timeout is (\\d+) seconds") { timeout: Int ->
            KrakenExchangeFactory.httpConnTimeout = timeout * 1000
            KrakenExchangeFactory.httpReadTimeout = timeout * 1000
        }

        Given("the server is not returning a response") {
            world.wireMockServer.stubFor(get(urlPathEqualTo("/0/public/OHLC"))
                    .willReturn(
                            aResponse()
                                    ?.withFixedDelay(60000)
                    )
            )
            world.wireMockServer.stubFor(post(urlPathEqualTo("/0/private/AddOrder"))
                    .willReturn(
                            aResponse()
                                    ?.withFixedDelay(60000)
                    )
            )
            world.wireMockServer.stubFor(WireMock.post(WireMock.urlEqualTo("/0/private/Balance"))
                    .willReturn(
                            aResponse()
                                    ?.withFixedDelay(60000)
                    )
            )
        }
        
        Given("the initial amount of \"(\\w+)\" is \"(\\d+.?\\d*)\" for \"(\\w+)\"`s wallet") {
            currency: String, amount: Double, name: String ->

            if(!world.initalAmountsMocks.containsKey(name)) {
                world.initalAmountsMocks[name] = jsonObject(
                        "error" to jsonArray(),
                        "result" to jsonObject()
                )
            }
            world.initalAmountsMocks[name]?.getAsJsonObject("result")
                    ?.addProperty(currency, BigDecimal(amount).setScale(8, RoundingMode.HALF_UP).toPlainString())
        }

        Given("I have \"(\\w+)\" registered as a trader") { trader: String -> 
            this.world.trader = Trader("Julien",
                    "mSWSjO3xUBQYPBQCt1pB8T9LyLoY1JA39h4rHi8GQY5dbR+Eih/zj+nF",
                    "kt8Q3kpenQ630Bevd6XhB2xA34/QQsZDoP20wnys6G2ctIF+6XXqM+FLsdAY1PWfeFcqbpDhe7OI+9W+HTV39Q==")
        }

        Given("the current \"(\\w+_\\w+)\" value is \"(\\d+.?\\d*)\"") { pair: String, value: Double ->

            val currencyPair = CurrencyPair(pair.replace("_", "/"))
            val ticker = "X" + currencyPair.base.iso4217Currency + "Z" + currencyPair.counter.iso4217Currency

            world.wireMockServer.stubFor(get(urlEqualTo("/0/public/OHLC?pair=$ticker&interval=1&since=99999999999"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody("{\n" +
                                            "  \"error\": [],\n" +
                                            "  \"result\": {\n" +
                                            "    \""+ticker+"\": [\n" +
                                            "      [\n" +
                                            "        1599920700,\n" +
                                            "        \"0.0\",\n" +
                                            "        \"0.0\",\n" +
                                            "        \"0.0\",\n" +
                                            "        \""+value+"\",\n" +
                                            "        \"0.0\",\n" +
                                            "        \"0.0\",\n" +
                                            "        0\n" +
                                            "      ]\n" +
                                            "    ],\n" +
                                            "    \"last\": 1599920640\n" +
                                            "  }\n" +
                                            "}")
                    )
            )

            world.ohlcs[currencyPair] = null // remember the currency pair, value will be fetched in When step
        }

        Given("the \"(\\w+)\" allocation is \"(\\d+.?\\d*)\"") {
            currency: String, valuePercent: Double ->

            AllocationsResultsRepository().persist(listOf(AllocationResult(
                    currency = currency,
                    valuePercent = valuePercent,
                    allocationName = "SOME_ALLOCATION_MODEL"
            )))
        }

        Given("the minimum order size for \"(\\w+)\" is \"(\\d+.?\\d*)\"") {
            currency: String, minOrderSize: Double ->

            this.world.minimumOrderSizes[Currency(currency)] = BigDecimal(minOrderSize)
        }

        Given("the server is available") {

            world.wireMockServer.stubFor(post(urlEqualTo("/0/private/AddOrder"))
                    .willReturn(
                            aResponse()
                                    ?.withHeader("Content-Type", "application/json")
                                    ?.withBody("{\n" +
                                            "  \"error\": [\n" +
                                            "    \n" +
                                            "  ],\n" +
                                            "  \"result\": {\n" +
                                            "    \"descr\": {\n" +
                                            "      \"order\": \"buy 0.00100000 XBTEUR @ market\"\n" +
                                            "    },\n" +
                                            "    \"txid\": [\n" +
                                            "      \"OT3SFZ-KX6GD-5LYIGH\"\n" +
                                            "    ]\n" +
                                            "  }\n" +
                                            "}")
                    )
            )
        }

        Given("the \"(\\w+)\" delta is \"(-?\\d+.?\\d?)\" for \"(\\w+)\"`s wallet") {
            currency: String, delta: Double, traderName: String ->

            if(!world.deltas.containsKey(traderName)) {
                world.deltas[traderName] = arrayListOf()
            }
            var oldDeltas: ArrayList<Delta> = world.deltas[traderName]!!
            oldDeltas.add(Delta(Currency(currency), BigDecimal(delta)))
            world.deltas[traderName] = oldDeltas

        }

        Given("^the \"(\\w+_\\w+)\" currency is marked as \"([\\w_]+)\"") { pair: String, currentStatus: String ->
            InvestmentUniverseStatusUpdater(pair, currentStatus)
        }
    }
}