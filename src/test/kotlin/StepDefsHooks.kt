import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import cucumber.api.Scenario
import cucumber.api.java8.En
import kraken.KrakenConnector


class StepDefsHooks(var world: World, val krakenConnector: KrakenConnector): En {

    init {

        Before(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer = WireMockServer()
            world.wireMockServer.start()
            world.stubResponses = mapOf()

            krakenConnector.setUp()
        }

        Before(arrayOf("@InvestmentCalculator")) { scenario: Scenario ->
            println("DEBUG: world.errorOccured = "+world.errorOccured)
        }

        After(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer.stop()
        }
    }
}