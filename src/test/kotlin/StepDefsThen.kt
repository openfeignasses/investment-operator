import cucumber.api.java8.En
import model.Delta
import org.knowm.xchange.currency.Currency
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.dto.account.Balance
import repositories.InvestmentUniverseStatusRetriever
import java.math.BigDecimal
import java.math.RoundingMode


class StepDefsThen(var world: World): En {

    init {

        Then("^an error is thrown$") {
            assert(world.errorOccured)
        }

        Then("the final amount of \"(\\w+)\" is \"(\\d+.?\\d*)\" for \"(\\w+)\"`s wallet") {
            currency: String?, expectedAmount: Double, traderName: String ->

            val initialAmount: BigDecimal = world.initialWallets[traderName]?.balances?.getOrDefault(Currency(currency),
                    Balance.zero(Currency(currency)))?.available ?: BigDecimal.ZERO
            var delta = BigDecimal.ZERO
            if (Currency(currency) == Currency.EUR) {
                world.deltas[traderName]!!.forEach {
                    val currencyDelta = world.ohlcs[CurrencyPair(it.currency, Currency.EUR)]!!.close * it.value
                    delta -= currencyDelta
                }
            } else {
               delta = findDelta(world.deltas[traderName]!!, Currency(currency))
            }
            val finalAmount = initialAmount + delta
            println("final amount calculated for "+currency+": "+finalAmount!!.setScale(8, RoundingMode.HALF_UP))
            println("expectedAmount : "+BigDecimal(expectedAmount).setScale(8, RoundingMode.HALF_UP))
            assert(finalAmount!!.setScale(8, RoundingMode.HALF_UP) ==
                    BigDecimal(expectedAmount).setScale(8, RoundingMode.HALF_UP))
        }

        Then("\"(\\w+)\"`s wallet has been re-balanced") {
            traderName: String ->

            assert(world.walletRebalanced[traderName]!!)
        }

        Then("the \"(\\w+_\\w+)\" currency is flagged as \"([\\w_]+)\"") { pair: String, expectedStatus: String ->
            assert(InvestmentUniverseStatusRetriever().findByPair(pair).status == expectedStatus)
        }
    }

    private fun findDelta(deltas: List<Delta>, currency: Currency): BigDecimal {
        val delta = deltas.find { it.currency == currency }
        if (delta === null) {
            return BigDecimal(0)
        }

        return delta.value
    }
}